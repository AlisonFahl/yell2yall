﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yell2Yall.Data.Models
{
    public class Message
    {
        public virtual ulong Id { get; set; }
        public virtual string Text { get; set; }
        public virtual double TimeStamp { get; set; }
        public virtual uint Ip { get; set; }
    }
}
