﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using NHibernate;

namespace Yell2Yall.Data.Reposiroty
{
    public class BaseRepository<T>
    {
        private ISession m_Session;

        public BaseRepository(ISession session)
        {
            m_Session = session;
        }

        public IQueryable<T> Queryable
        {
            get
            {
                return m_Session.Query<T>();
            }
        }

        public void Save(T entity)
        {
            m_Session.SaveOrUpdate(entity);
        }

        public void Delete(T entity)
        {
            m_Session.Delete(entity);
        }
    }
}
