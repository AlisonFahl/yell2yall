﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yell2Yall.Data.Models;

namespace Yell2Yall.Data.Mappings
{
    public class MessageMap: ClassMap<Message>
    {
        public MessageMap()
        {
            Table("Messages");

            Id(x => x.Id).GeneratedBy.Increment();
            Map(x => x.Text);
            Map(x => x.TimeStamp);
            Map(x => x.Ip);
        }
    }
}
