﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Yell2Yall.Data.Mappings;

namespace Yell2Yall
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        public static ISessionFactory SessionFactory {get; private set;}

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //Create SessionFactory
            SessionFactory = Fluently.Configure()
                .Database(MySQLConfiguration.Standard.ConnectionString("Server=mysql2.gear.host;Database=hangfirejobs;Uid=hangfirejobs;Pwd=gearhostisthebest!;"))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<MessageMap>())
                .BuildSessionFactory();
        }
    }
}