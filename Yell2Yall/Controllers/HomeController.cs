﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Yell2Yall.Business.Services;
using Yell2Yall.Data.Reposiroty;

namespace Yell2Yall.Controllers
{
    public class HomeController : BaseController
    {
        //
        // GET: /Home/

        private MessageService m_MessageService;

        public HomeController()
        {
            m_MessageService = new MessageService(new MessageRepository(DBSession));
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PostMessage(string text)
        {
            var ip = Request.UserHostAddress;

            int intIp = BitConverter.ToInt32(IPAddress.Parse(ip).GetAddressBytes(), 0);

            var result = m_MessageService.PostMessage(text, intIp);

            return Json(new 
            {
                code = result ? 200 : 0,
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRecentMessages(ulong lastMsgId)
        {
            return Json(m_MessageService.GetRecentMessages(lastMsgId), JsonRequestBehavior.AllowGet);
        }

    }
}
