﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Yell2Yall.Controllers
{
    public class BaseController : Controller
    {
        private ITransaction m_Transaction;
        private ISession m_Session;

        protected ISession DBSession { get { return m_Session; } }

        public BaseController()
        {
            m_Session = MvcApplication.SessionFactory.OpenSession();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            m_Transaction = m_Session.BeginTransaction();

            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            try
            {
                m_Transaction.Commit();
            }
            catch(Exception)
            {
                m_Transaction.Rollback();
            }

            m_Transaction.Dispose();
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                m_Session.Close();
                m_Session.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
