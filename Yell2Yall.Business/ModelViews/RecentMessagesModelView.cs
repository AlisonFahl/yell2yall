﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yell2Yall.Business.ModelViews
{
    public class RecentMessagesModelView
    {
        public ulong Id { get; set; }
        public string Date { get; set; }
        public string Text { get; set; }
    }
}
