﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yell2Yall.Data.Models;
using NHibernate.Linq;
using Yell2Yall.Data.Reposiroty;
using Yell2Yall.Business.ModelViews;

namespace Yell2Yall.Business.Services
{
    public class MessageService
    {
        private const uint MINIMUM_INTERVAL = 1000 * 60;

        private MessageRepository m_Repository;

        public MessageService(MessageRepository repository)
        {
            m_Repository = repository;
        }

        public bool PostMessage(string text, int ip)
        {
            DateTime time = DateTime.UtcNow;
            bool hasPostedRecently = false;

            //Check if the user has already sent a message in the last MINIMUM_INTERVAL

            //Gets any message comming from the given ip address
            var records = m_Repository.Queryable.Where(x => x.Ip == ip);
            //check if any of them is under MINIMUM_INTERVAL
            if(records.Count() > 0)
            {
                //cast to list because NHibernate.Linq does not support opperations over TimeSpan
                hasPostedRecently = records.ToList().Any(x =>
                    {
                        var timeElapsed = time - new DateTime().AddMilliseconds(x.TimeStamp);
                        return timeElapsed.TotalMilliseconds < MINIMUM_INTERVAL;
                    });
            }

            //deny post if the given ip has posted something recently. Save post otherwise
            if(hasPostedRecently)
            {
                return false;
            }
            else
            {
                Message message = new Message()
                {
                    Text = text,
                    Ip = (uint)ip,
                    TimeStamp = new TimeSpan(time.Ticks).TotalMilliseconds
                };

                m_Repository.Save(message);

                return true;
            }
        }

        public List<RecentMessagesModelView> GetRecentMessages(ulong lastMsgId)
        {
            var records = m_Repository.Queryable.Where(x => x.Id > lastMsgId).ToList();

            var views = records.Select(x => new RecentMessagesModelView()
                {
                    Id = x.Id,
                    Date = new DateTime().AddMilliseconds(x.TimeStamp).ToString(),
                    Text = x.Text
                }).ToList();

            return views;
        }
    }
}
